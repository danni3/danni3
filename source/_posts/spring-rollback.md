---
title: 终于可以在spring里回滚
date: 2009-05-13 20:45:21
tags: spring
---
![](http://danni3.gitee.io/images/niagara.jpg)
今天才发现自己spring用得太次了。搞了一天才把一个小问题搞掂，还加班了。
 
# 1、装载beans配置文件
在web.xml里配置你的xml文件，然后加上一个监听器：
```
 <context-param>
  <param-name>contextConfigLocation</param-name>
  <param-value>classpath:bean.xml</param-value>
 </context-param>
 <listener>
  <listener-class>
   org.springframework.web.context.ContextLoaderListener
  </listener-class>
 </listener>
 ```
或者直接在代码里写
ApplicationContext ctx=new FileSystemXmlApplicationContext("/bean.xml");
 
这两者当然是不一样的。
 
# 2、在spring里使用jdbc模板
xml里配置bean：
```
 <bean id="billingDS"
  class="org.springframework.jndi.JndiObjectFactoryBean">
  <property name="jndiName" value="billing" />
 </bean>
 
 <bean id="jdbcTemplate"
  class="org.springframework.jdbc.core.JdbcTemplate">
  <property name="dataSource" ref="billingDS" />
 </bean>
 ```
当然，首先要在weblogic里配数据源和连接池。
 
然后就可以直接用了，比如：
```
 <bean id="jtaTest" class="spring.jta.Test">
  <property name="jdbcTemplate">
   <ref bean="jdbcTemplate" />
  </property>
 </bean>
 ```
# 3、代码里面要配一个jdbcTemplate的setter方法
以前一直不知道怎么自动生成，现在发现了：
private JdbcTemplate jdbcTemplate = null;
在eclipse里，选中jdbcTemplate ，点击Source--Generate Getters and Setters，会弹出一个框，勾上Setter点确定就ok。
 
Source里很多好用的东西，比如一个方法要import它的类，点一下Source--Add Import；代码写得乱七八糟的，点一下Source--Format立刻就整齐。
 
# 4、在spring里使用任务调度
spring提供了两种，一个timer，一个quartz，后者相对功能强大点。因为前者只可以配置间隔多少时间定时执行；后者既可以实现该点，也可以实现unix里的crontab功能，固定在几月几日几点几分几秒执行。
如果用timer，配三个bean：
```
 <bean id="scheduler"
  class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
  <property name="triggers">
   <list> 
    <ref bean="simpleTrigger" />
   </list>
  </property>
  <property name="schedulerContextAsMap">
   <map>
    <entry key="timeout" value="30" />
   </map>
  </property>
 </bean>
 <bean id="simpleTrigger"
  class="org.springframework.scheduling.quartz.SimpleTriggerBean">
  <property name="jobDetail">
   <ref bean="DataProxyJob" />
  </property>
  <property name="startDelay">
   <value>5000</value>
  </property>
  <property name="repeatInterval">
   <value>2000</value>
  </property>
 </bean>
 <bean id="DataProxyJob"
  class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
  <property name="targetObject" ref="jtaTest" />
  <property name="targetMethod" value="doJob" />
  <property name="concurrent" value="false" />
 </bean>
 ```
如果用quartz，也是三个bean，其中第一个scheduler是不变的：
```
 <bean id="DataProxyJob"
  class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
  <property name="targetObject" ref="jtaTest" />
  <property name="targetMethod" value="doJob" />
  <property name="concurrent" value="false" />
 </bean>
 <bean id="simpleTrigger"
  class="org.springframework.scheduling.quartz.CronTriggerBean">
  <property name="jobDetail" ref="DataProxyJob" />
  <property name="cronExpression" value="0/6 * * * * ?" />
 </bean>
 ```
# 5、在spring里使用事务
终于讲到我题目这个问题了！
就是这个问题搞了我一天。
spring的事务还是比较强大，我这里只用到DataSourceTransactionManager
先配一个proxy：
```
 <bean id="HBtransactionManager"
  class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
  <property name="dataSource">
   <ref local="billingDS" />
  </property>
 </bean>
 <bean id="HBtransactionProxy"
  class="org.springframework.transaction.interceptor.TransactionProxyFactoryBean"
  abstract="true">
  <property name="transactionManager">
   <ref bean="HBtransactionManager" />
  </property>
  <property name="transactionAttributes">
   <props>
    <prop key="doJob*">PROPAGATION_REQUIRED</prop>
    <prop key="*">PROPAGATION_SUPPORTS,readOnly</prop>
   </props>
  </property>
 </bean>
 ```
解释一下，doJob*的意思是以doJob打头的方法，比如doJob、doJob1都是；PROPAGATION_REQUIRED的意思是没有事务就起一个事务，有事务就使用当前事务，很常用的一个选项，另外还有6个就不详细说了。注意，这里的方法是调用这个类的第一个方法，我试过，如果先执行methodA，methodA里再调用doJob方法是不行的。
另外，还发现了在doJob里使用try、catch也是不行的！时间紧，后天就上线了，我也没时间搞懂为什么。高手指点一下。
后来我是先用classA，里面写try、catch，通过classA调用classB里的doJob方法，这样classB就不需要写try，而也能通过classA捕捉异常，doJob里也能事务回滚。可惜本来只配一个bean变成配两个。
 
使用这个代理：
```
 <bean id="jtaTest" parent="HBtransactionProxy">
  <property name="target">
   <bean class="spring.jta.Test" autowire="byName"/>
  </property>
 </bean>
```
