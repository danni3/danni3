---
title: plsql快捷键
date: 2009-07-08 12:43:02
tags: oracle
---

![](http://danni3.gitee.io/images/music2.jpg)

一直看见同事在plsql里输入一两个字母，屏幕上竟然显示的是：
> select t.*,rowid from
 
非常惊讶，上网搜索，始终找不到答案。
网上竟然有位“高手”说，按的是ctrl+v，所以这么快，我实在无语。
 
今天偶尔在plsql7.1.4版本上，竟然真的实现了！原来只是我的7.0版本不支持而已。
 
配置方法：
> tools-preferences-user interface-editor-autoreplace

点一下edit，弹出一个框：
```
sf = select * from 
s = select 
sc = select count(*) from 
sr = select t.*,rowid from
```
就是如此的简单，困扰了我好久。
 
另外，如果没有装oracle，只是拷了一个配置文件夹的话，配置地方：
> tools-preferences-oracle-connection

oracle hone填上：
> D:\oracleclient-basic-win32-11.1.0.6.0\instantclient_11_1\network\admin

oci libarary填上：
> D:\oracleclient-basic-win32-11.1.0.6.0\instantclient_11_1\oci.dll
