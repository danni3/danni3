---
title: 在Vue.js里使用ECharts
date: 2019-02-13 15:13:25
tags: 前端
---

![](http://danni3.gitee.io/images/echarts.jpg)

# 简介

- 数据库可视化echarts我国比较流行，百度的，开源、免费
- 开源中国2018年第二
- 其他类似
  - 外国还有highecharts，收费
  - 免费的外国还有D3.js等

# 官网

- 最新版本4.2.1-rc.1
  - https://echarts.baidu.com
  - https://echarts.baidu.com/download.html
- 旧版本echarts2、3
  - https://echarts.baidu.com/echarts2/
  - https://echarts.baidu.com/download3.html
- api
  - https://www.echartsjs.com/api.html#echarts
- option配置项
  - https://www.echartsjs.com/option.html#title

# 在vue里使用

## 普通图形

以柱状图举例。

- 首先要安装echarts `npm install echarts`
- import echarts from 'echarts'
- 定义div
- echarts初始化div
- 定义option
- 设置option

## 完整代码

xxx.vue
```vue
<template>
  <div>
    <div id="chart_example">
    </div>
  </div>
</template>

<script>
  // 首先要 cnpm install echarts
  import echarts from 'echarts'

  export default {
    data() {
      return {}
    },
    mounted() {
      let myChart = echarts.init(document.getElementById('chart_example'));

      /**
       * 简单柱状图
       * https://echarts.baidu.com/examples/editor.html?c=bar-simple
       */
      let option = {
        xAxis: {
          type: 'category',
          data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        },
        yAxis: {
          type: 'value'
        },
        series: [{
          data: [120, 200, 150, 80, 70, 110, 130],
          type: 'bar'
        }]
      };

      myChart.setOption(option);
    },
    methods: {}
  }
</script>

<style scoped>
  #chart_example {
    height: 500px;/*必须有，否则出不来*/
  }
</style>
```

## 效果

![](http://danni3.gitee.io/images/echarts-bar.jpg)
