---
title: 60行代码实现socket的服务端和客户端
date: 2008-05-01 17:15:47
tags: c/c++
---
![长城·2007.8](https://danni3.gitee.io/images/huangpu.jpg "changcheng.jpg")

今天加班，随便写个日志。
 
已经尽量把代码减到最少，我觉得这样学是最容易的，长连接的，有注释。
 
先来个客户端，只需20行。
 ```
#include <sys/socket.h>
#include <arpa/inet.h>
int main(int argc, char **argv)
{
 int sock;
 char sendbuf[15];
 struct sockaddr_in IPaddr;
 
 sock = socket(AF_INET, SOCK_STREAM, 0);/*获得套接口描述字*/
 
 (void)bzero(&IPaddr, sizeof(IPaddr));
 
 IPaddr.sin_family = AF_INET;/*IPv4*/
 IPaddr.sin_addr.s_addr = inet_addr("10.156.76.43");/*ip*/
 IPaddr.sin_port = htons(7086);/*端口*/
 
  connect(sock, (struct sockaddr *)&IPaddr, sizeof(IPaddr));/*三次握手*/
  
 sprintf(&sendbuf[0], "%s", "oyd_haha") ;
 
 while (1) 
 {
  write(sock, &sendbuf[0], 8);/*写数据*/
  sleep(1);
 }
}
 ```
再来个服务端，多点，40行。
```
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
int main(int argc, char **argv)
{
 int sfd;
 int cfd;
 fd_set rfds;
 struct sockaddr_in laddr;
 struct sockaddr_in raddr;
 unsigned long ullocalIP;
 unsigned long addrlen ;
 struct timeval tval;
 int nConnFd ;
 char szRecvBuf[150] ;
 char recvhead[8 + 1];
 
 sfd = socket(AF_INET, SOCK_STREAM, 0);
 
 memset(&laddr, 0, sizeof(laddr));
 ullocalIP=inet_addr("10.156.76.43");
 laddr.sin_family = AF_INET;
 laddr.sin_port = htons(7086);
 laddr.sin_addr.s_addr = 
  (ullocalIP != 0xFFFFFFFF) ? htonl(ullocalIP) : htonl (INADDR_ANY);
 bind(sfd, (struct sockaddr*)&laddr, sizeof(laddr));/*分配本地IP和端口*/
 
 listen(sfd, 1);/*主动套接口转换成被动套接口*/
 
 while(1)
 {
  FD_ZERO(&rfds);/*清空端口集*/
  FD_SET(sfd, &rfds);/*设置监听的端口数目*/
  tval.tv_sec = 60;/*监听时间，秒*/
  tval.tv_usec = 0;
  
  select(sfd + 1, &rfds, NULL, NULL, &tval);/*监听*/
  
  if(FD_ISSET(sfd, &rfds))
  {
   cfd = accept(sfd, (struct sockaddr*)&raddr, &addrlen);/*返回连接*/
   
   while(1)
   {
    read(cfd, &szRecvBuf[0], 8);/*读数据*/
    printf("read_succeed:[%s]\n",szRecvBuf);
   }
  }
 }
}
```

![](https://images.gitee.com/uploads/images/2019/0526/102358_3d1f8252_2109404.jpeg)