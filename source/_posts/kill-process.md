---
title: 杀进程
date: 2009-09-08 16:26:11
tags: shell
---

![](http://danni3.gitee.io/images/china-great-wall.jpg)

不记下来总忘记。
杀进程：
```
$ cat k.sh
pid=`ps -ef|grep "daemon gzcrm"|grep -v grep|awk '{print $2}'`
echo pid=$pid
kill -9 $pid
echo kill end.
```
刚开始我用kill.sh，总报错：
```
$ kill.sh
usage: grep [-r] [-R] [-H] [-L] [-E|-F] [-c|-l|-q] [-insvxbhwy] [-p[parasep]] -e pattern_list...
        [-f pattern_file...] [file...]
usage: grep [-E|-F] [-c|-l|-q] [-insvxbhwy] [-p[parasep]] [-e pattern_list...]
        -f pattern_file... [file...]
usage: grep [-E|-F] [-c|-l|-q] [-insvxbhwy] [-p[parasep]] pattern_list [file...]
```
才知道kill.sh这个名字已经被系统用了。
 
删除文件夹：
`rm -R score`
 
拷贝文件夹：
`cp -r score`
