---
title: jBPM7入门
date: 2019-03-17 10:28:00
tags: workflow
---

![](https://danni3.gitee.io/images/jbpm7.png)

# jBPM7入门

## 简介

工作流与spring、前端等相比，本来就相对冷门，网上资料不多，activiti的相对较多，jbpm较少，估计其他的工作流就更少了。
jbpm与Drools一样，属于jboss，《疯狂的工作流讲义》（2018年1月出版）里说它使用的开源协议也比较限制，LGPL，也就是可以随便商业使用，但一旦基于它二次开发了，就必须也开源。（注意：其实并不是，见下更正）
activit则使用apache协议（下载后从README可以看到），更为开放，可以二次开发用作商业，而不必开源。

所以大家更偏向选择activiti5、6、7或者flowable5、6。(jbpm3、4的开源协议没研究)
因为一脉相承的顺序是：jbpm3 - jbpm4 - activiti5 - flowable6。
工作流的圈子真复杂，总是喜欢另起炉灶。
对手activiti版本出到7了，jbpm当然也不能落后，目前最新版本是7.18
（jboss是被redhat收购的，所以jbpm官网上会看见redhat）

> 下载地址：http://www.jbpm.org/download/download.html

提供五个下载包：
```
The jBPM binaries include documentation, examples and sources.
download Download jBPM 7.18.0.Final binaries
jBPM 7.18.0.Final-bin.zip
jBPM 7.18.0.Final-examples.zip
jBPM-installer 7.18.0.Final.zip
jBPM-installer-full 7.18.0.Final.zip
updatesite 7.18.0.Final
```

下载jBPM 7.18.0.Final-examples.zip，导入IntelliJ。自动下载maven库里jar，比较慢。

看看example里面quickstart的代码：

```java
package org.jbpm.examples.quickstarts;

public class HelloService {
	private static final HelloService INSTANCE = new HelloService();
	public static HelloService getInstance() {
		return INSTANCE;
	}
	public void sayHello(String name) {
		System.out.println("Hello " + name);
	}
}
```

bpmn流程代码：
```bpmn
<?xml version="1.0" encoding="UTF-8"?> 
<definitions id="Definition"
             typeLanguage="http://www.java.com/javaTypes"
             expressionLanguage="http://www.mvel.org/2.0"
             xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd"
             xmlns:g="http://www.jboss.org/drools/flow/gpd"
             xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
             xmlns:dc="http://www.omg.org/spec/DD/20100524/DC"
             xmlns:di="http://www.omg.org/spec/DD/20100524/DI"
             xmlns:tns="http://www.jboss.org/drools">

  <itemDefinition id="_personItem" structureRef="org.jbpm.examples.quickstarts.Person" />

  <process processType="Private" isExecutable="true" id="com.sample.script" name="Sample Process" tns:packageName="defaultPackage" >

    <extensionElements>
     <tns:import name="org.jbpm.examples.quickstarts.HelloService" />
    </extensionElements>
    <!-- process variables -->
    <property id="person" itemSubjectRef="_personItem"/>

    <!-- nodes -->
    <startEvent id="_1" name="StartProcess" />
    <scriptTask id="_2" name="Script" >
      <script>HelloService.getInstance().sayHello(person.getName());</script>
    </scriptTask>
    <endEvent id="_3" name="End" >
        <terminateEventDefinition/>
    </endEvent>

    <!-- connections -->
    <sequenceFlow id="_1-_2" sourceRef="_1" targetRef="_2" />
    <sequenceFlow id="_2-_3" sourceRef="_2" targetRef="_3" />

  </process>

  <bpmndi:BPMNDiagram>
    <bpmndi:BPMNPlane bpmnElement="com.sample.script" >
      <bpmndi:BPMNShape bpmnElement="_1" >
        <dc:Bounds x="16" y="16" width="48" height="48" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="_2" >
        <dc:Bounds x="96" y="16" width="80" height="48" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="_3" >
        <dc:Bounds x="208" y="16" width="48" height="48" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge bpmnElement="_1-_2" >
        <di:waypoint x="40" y="40" />
        <di:waypoint x="136" y="40" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="_2-_3" >
        <di:waypoint x="136" y="40" />
        <di:waypoint x="232" y="40" />
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>

</definitions>
```

测试类代码：
```java
/*
 * Copyright 2017 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jbpm.examples.quickstarts;

import java.util.HashMap;
import java.util.Map;

import org.jbpm.test.JbpmJUnitBaseTestCase;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * This is a sample file to test a process.
 */
public class JavaServiceQuickstartTest extends JbpmJUnitBaseTestCase {

	@Test
	public void testProcess() {
		KieSession ksession = createRuntimeManager("quickstarts/ScriptTask.bpmn").getRuntimeEngine(null).getKieSession();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("person", new Person("krisv"));
		ksession.startProcess("com.sample.script", params);
	}

}

```

执行test，运行成功。控制台打印出：`Hello krisv`

从代码可以知道：
- 如何进行一个简单的调用和测试
- jbpm使用的也是bpmn2.0
- 从注释知道属于redhat，并且协议是apacheV2（此处是example，不一定是jbpm）
- 于是我抱着怀疑态度，重新看了jbpm的代码，发现协议确实是apache，杨大仙的《疯狂的工作流讲义》看来要更新了，使用的协议并不是LGPL。
- https://github.com/kiegroup/jbpm/blob/master/LICENSE-ASL-2.0.txt
- 或者随便看其中一个java文件的开头：https://github.com/kiegroup/jbpm/blob/master/jbpm-bpmn2/src/main/java/org/jbpm/bpmn2/core/Association.java


## 源代码

- https://github.com/kiegroup/jbpm
- 曾经这么出名的名字，现在只有区区890个star，还不如专心搞drools（也就2028star）

## 总结

- jbpm比较麻烦，下载东西比较多，还得ant安装什么的（太麻烦我没安装）
- 而activiti只需要部署个war包到tomcat
- jbpm提供了example，直接可以运行，学习挺好；activiti则没有提供
- jbpm究竟什么时候用的LGPL找不到，现在找到的都是说用ASL，估计LGPL是2009年以前的事情

## 相关文章

- [纵观 jBPM：从 jBPM3 到 jBPM5 以及 Activiti5-荣浩](https://infoq.cn/article/rh-jbpm5-activiti5)
- [jBPM5 vs Actitivi](http://www.blogways.net/blog/2013/07/16/activiti-jbpm-compare.html)

## 附录

jbpm的release版本。官网已找不到5的版本。
- https://download.jboss.org/jbpm/release/

Index of /jbpm/release

[ICO]	Name	Last modified	Size	Description
[DIR]	Parent Directory	 	-	 
[DIR]	6.0.0.Final/	21-Dec-2017 17:18	-	 
[DIR]	6.0.1.Final/	11-Mar-2016 10:31	-	 
[DIR]	6.1.0.Final/	11-Mar-2016 10:52	-	 
[DIR]	6.2.0.Final/	11-Mar-2016 10:52	-	 
[DIR]	6.3.0.Final/	11-Mar-2016 11:11	-	 
[DIR]	6.3.1.Final/	17-Mar-2016 17:47	-	 
[DIR]	6.4.0.Beta1/	11-Mar-2016 09:26	-	 
[DIR]	6.4.0.Beta2/	11-Mar-2016 09:55	-	 
[DIR]	6.4.0.CR1/	11-Mar-2016 09:57	-	 
[DIR]	6.4.0.CR2/	24-Mar-2016 14:10	-	 
[DIR]	6.4.0.Final/	15-Apr-2016 10:45	-	 
[DIR]	6.4.1.Final/	03-May-2016 10:28	-	 
[DIR]	6.5.0.Beta1/	05-Aug-2016 08:25	-	 
[DIR]	6.5.0.CR1/	26-Aug-2016 05:39	-	 
[DIR]	6.5.0.CR2/	14-Sep-2016 11:30	-	 
[DIR]	6.5.0.Final/	21-Oct-2016 08:53	-	 
[DIR]	7,0,0,Beta7/	07-Mar-2017 13:25	-	 
[DIR]	7.0.0.Beta1/	06-Jul-2016 10:49	-	 
[DIR]	7.0.0.Beta2/	03-Oct-2016 13:23	-	 
[DIR]	7.0.0.Beta3/	21-Nov-2016 06:45	-	 
[DIR]	7.0.0.Beta4/	07-Dec-2016 05:09	-	 
[DIR]	7.0.0.Beta5/	22-Dec-2016 11:26	-	 
[DIR]	7.0.0.Beta6/	30-Jan-2017 10:58	-	 
[DIR]	7.0.0.Beta7/	07-Mar-2017 14:25	-	 
[DIR]	7.0.0.Beta8/	31-Mar-2017 08:23	-	 
[DIR]	7.0.0.CR1/	07-Apr-2017 11:27	-	 
[DIR]	7.0.0.CR2/	26-Apr-2017 11:06	-	 
[DIR]	7.0.0.CR3/	03-May-2017 09:38	-	 
[DIR]	7.0.0.Final/	09-Jun-2017 10:27	-	 
[DIR]	7.0.1.Final/	06-Jun-2017 11:41	-	 
[DIR]	7.1.0.Beta2/	01-Jun-2017 11:09	-	 
[DIR]	7.1.0.Beta3/	04-Jul-2017 07:44	-	 
[DIR]	7.1.0.Final/	19-Jul-2017 08:20	-	 
[DIR]	7.2.0.Final/	17-Aug-2017 08:23	-	 
[DIR]	7.3.0.Final/	12-Sep-2017 11:13	-	 
[DIR]	7.4.1.Final/	09-Nov-2017 10:50	-	 
[DIR]	7.5.0.Final/	12-Dec-2017 05:37	-	 
[DIR]	7.6.0.Final/	06-Feb-2018 22:24	-	 
[DIR]	7.7.0.Final/	05-Apr-2018 06:41	-	 
[DIR]	7.8.0.Final/	05-Jul-2018 09:12	-	 
[DIR]	7.9.0.Final/	27-Jul-2018 04:25	-	 
[DIR]	7.10.0.Final/	17-Aug-2018 06:01	-	 
[DIR]	7.11.0.Final/	10-Sep-2018 12:33	-	 
[DIR]	7.12.0.Final/	01-Oct-2018 12:15	-	 
[DIR]	7.13.0.Final/	19-Oct-2018 07:11	-	 
[DIR]	7.14.0.Final/	14-Nov-2018 06:02	-	 
[DIR]	7.15.0.Final/	29-Nov-2018 11:19	-	 
[DIR]	7.16.0.Final/	14-Jan-2019 04:53	-	 
[DIR]	7.17.0.Final/	31-Jan-2019 05:30	-	 
[DIR]	7.18.0.Final/	07-Mar-2019 04:27	-	 
[DIR]	latest/	07-Mar-2019 04:27	-	 
[DIR]	latestFinal/	07-Mar-2019 04:27	-	 
[DIR]	snapshot/	28-Jan-2016 15:00	-	 