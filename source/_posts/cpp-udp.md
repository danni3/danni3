---
title: cpp-udp
date: 2009-09-08 18:40:15
tags: c/c++
---

```
DP = /oravl01/crmdvp/product/develop/UNIX_DEV/jifen/demo4daemon
PUB_LIB = $(DP)/public/lib
PUB_INC = $(DP)/public/include
CTRL_BIN = $(DP)/bin
INCLUDES = -I. -I$(PUB_INC) -Iinclude -I$(ORACLE_HOME)/precomp/public \
 -I$(ORACLE_HOME)/rdbms/demo -I$(ORACLE_HOME)/rdbms/public
PUBFLAGS = -L$(ORACLE_HOME)/lib -lclntsh -lpthread -lm -lc -lnsl -lrt -locci -ldl
PCFLAGS =-qthreaded $(INCLUDES) -DAIX -q64 -DNAMESPACE -bhalt:5 -qstaticinline
PLDFLAGS = $(PUBFLAGS) -L$(PUB_LIB) -lpublic
CC = xlC_r -w 
STRIP = strip -X64
CFLAGS = $(PCFLAGS) -O2
LDFLAGS = $(PLDFLAGS)
OBJS = main.o
TARGET = demo
all:$(TARGET) install
 
$(TARGET):$(OBJS)
 $(CC) $(CFLAGS) -o $@ $(OBJS) $(LDFLAGS)
.SUFFIXES:.o .h
.SUFFIXES:.cpp .o
.cpp.o:
 $(CC) -c $(CFLAGS) -DDEBUG -o  $@ $<
clean:
 rm -f *.o
install:
 $(STRIP) $(TARGET)
 cp $(TARGET) $(CTRL_BIN)/
remake: clean all
 
#include <cstdio>
#include <signal.h>
#include <unistd.h>
#include <cstring>
#include <strings.h>
#include <fcntl.h>
#include <unistd.h>
#include <dlfcn.h>
#include <exception>
#include <iostream>
#include "common.h"
using util::database;
#if defined(NAMESPACE)
#endif
using std::cout;
using util::common;
using util::log_udp;
void do_signal(int sig_no)
{
    exit(0);
}
int main(int argc,char** argv)
{
    try
    {
        signal(SIGINT, do_signal);
        if (argc != 2)
        {
            cout << "Usage:" << argv[0] << " host_id\n" << std::endl;
            exit(1);
        }
        log_udp* ludp;
        ludp = new log_udp(8000);
        ludp->thread_report("progress", "thread_id", "msg");
    cout << "the program exit.\n" << std::endl;
        return 0;
    }
    catch (std::exception& ex)
    {
     cout << "In file(" << __FILE__
         << ") at line(" << __LINE__
         << ") " << ex.what() << '\n';
     return 1;
    }
}
```
 
makefile一定要是unix格式的，dos格式会报错。

![](http://danni3.gitee.io/images/shanghai.jpg)
