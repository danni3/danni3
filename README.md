# danni3

#### 介绍
Hexo生成个人博客。

#### 软件架构
软件架构说明:Hexo 读音：[hek so]

#### 安装教程

1. npm install -g hexo-cli
2. hexo -v
3. hexo init myblog
4. cd myblog
5. npm install
6. hexo g
7. hexo server
8. 访问http://localhost:4000

#### 使用说明

- 官网：https://hexo.io/zh-cn/docs/
- 好的文章：https://blog.csdn.net/sinat_37781304/article/details/82729029
- hueman主题官方示例：https://blog.zhangruipeng.me/hexo-theme-hueman/

1. 更换主题：_config.yml
`theme: landscape`
  - 主题下载：https://hexo.io/themes/
  - 目前共：238 items
  - 之前github用的主题：http://cgmartin.github.io/hexo-theme-bootstrap-blog/
  - 点击进去主题，要寻找到github的链接才行
  - 下载zip慢，直接：
  `DannideMacBook-Pro:git danni$ git clone https://github.com/ppoffice/hexo-theme-hueman`
  - clone也不行，最终连续尝试n次，用了几个小时，最终下载成功了
  - hexo-theme-hueman-master改为hueman
  - _config.yml.example改名为_config.yml（一般主题不用,比如amber，直接拷贝就可以，但这个要，详细见github的wiki）
  - 然后记得改.gitignore!这个作者太坑了，搞个这东西干啥。
  - 更好后直接hexo server就可以，不需要hexo g或者d等
  - 图片要用网上的，本地的感觉好像不行，看了别人也是用网上的，可以先自己传到一个地方
  - 我一些直接用别人的，和一些图片上传到gitee里就直接可以用
2. 更换网站标题
`title: Hexo`
3. 更换作者
`author: John Doe`
4. 改了配置要重启

- 写博客
`DannideMacBook-Pro:danni3 danni$ hexo new use-hexo
INFO  Created: ~/work/svn/git/danni3/source/_posts/use-hexo.md`

> date: 2019-02-09 15:13:52，如果不添加date，就会用当前时间，每天都变。

- 新启page
`hexo new page use-hexo`

- 新启草稿
`hexo new draft use-hexo`

- 草稿开发时调试
`hexo server --draft`

- 发布草稿到post里
`hexo publish draft newpage`

- 关于我
` hexo new page about`

#### 注意

- hexo自动生成的.gitignore里有public和db.json，这两个不需要上传到git，所以不要自己弄ignore，用它的

#### 附录

```
DannideMacBook-Pro:git danni$ hexo -v
hexo-cli: 1.1.0
os: Darwin 16.0.0 darwin x64
http_parser: 2.8.0
node: 10.13.0
v8: 6.8.275.32-node.36
uv: 1.23.2
zlib: 1.2.11
ares: 1.14.0
modules: 64
nghttp2: 1.34.0
napi: 3
openssl: 1.1.0i
icu: 62.1
unicode: 11.0
cldr: 33.1
tz: 2018e
```


```
DannideMacBook-Pro:git danni$ hexo init danni3
FATAL ~/work/svn/git/danni3 not empty, please run `hexo init` on an empty folder and then copy your files into it
FATAL Something's wrong. Maybe you can find the solution here: http://hexo.io/docs/troubleshooting.html
Error: target not empty
    at Context.initConsole (/Users/danni/.nodejs/lib/node_modules/hexo-cli/lib/console/init.js:30:27)
    at Context.tryCatcher (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/util.js:16:23)
    at Context.<anonymous> (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/method.js:15:34)
    at /Users/danni/.nodejs/lib/node_modules/hexo-cli/lib/context.js:44:9
    at Promise._execute (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/debuggability.js:313:9)
    at Promise._resolveFromExecutor (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/promise.js:483:18)
    at new Promise (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/promise.js:79:10)
    at Context.call (/Users/danni/.nodejs/lib/node_modules/hexo-cli/lib/context.js:40:10)
    at /Users/danni/.nodejs/lib/node_modules/hexo-cli/lib/hexo.js:68:17
    at tryCatcher (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/util.js:16:23)
    at Promise._settlePromiseFromHandler (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/promise.js:512:31)
    at Promise._settlePromise (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/promise.js:569:18)
    at Promise._settlePromise0 (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/promise.js:614:10)
    at Promise._settlePromises (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/promise.js:694:18)
    at Promise._fulfill (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/promise.js:638:18)
    at Promise._resolveCallback (/Users/danni/.nodejs/lib/node_modules/hexo-cli/node_modules/bluebird/js/release/promise.js:432:57)
```



```
DannideMacBook-Pro:danni3 danni$ hexo init myblog
INFO  Cloning hexo-starter to ~/work/svn/git/danni3/myblog
Cloning into '/Users/danni/work/svn/git/danni3/myblog'...
remote: Enumerating objects: 68, done.
remote: Total 68 (delta 0), reused 0 (delta 0), pack-reused 68
Unpacking objects: 100% (68/68), done.
Submodule 'themes/landscape' (https://github.com/hexojs/hexo-theme-landscape.git) registered for path 'themes/landscape'
Cloning into '/Users/danni/work/svn/git/danni3/myblog/themes/landscape'...
remote: Enumerating objects: 1, done.        
remote: Counting objects: 100% (1/1), done.        
remote: Total 867 (delta 0), reused 0 (delta 0), pack-reused 866        
Receiving objects: 100% (867/867), 2.55 MiB | 2.00 KiB/s, done.
Resolving deltas: 100% (459/459), done.
Submodule path 'themes/landscape': checked out '73a23c51f8487cfcd7c6deec96ccc7543960d350'
INFO  Install dependencies
yarn install v1.12.3
info No lockfile found.
[1/4] 🔍  Resolving packages...
warning hexo > titlecase@1.1.2: no longer maintained
[2/4] 🚚  Fetching packages...
[3/4] 🔗  Linking dependencies...
[4/4] 📃  Building fresh packages...
success Saved lockfile.
warning Your current version of Yarn is out of date. The latest version is "1.13.0", while you're on "1.12.3".
info To upgrade, run the following command:
$ curl --compressed -o- -L https://yarnpkg.com/install.sh | bash
✨  Done in 26.27s.
INFO  Start blogging with Hexo!
```


```
DannideMacBook-Pro:danni3 danni$ hexo g
INFO  Start processing
INFO  Files loaded in 311 ms
INFO  Generated: index.html
INFO  Generated: archives/index.html
INFO  Generated: fancybox/blank.gif
INFO  Generated: fancybox/fancybox_loading.gif
INFO  Generated: fancybox/jquery.fancybox.css
INFO  Generated: fancybox/fancybox_sprite@2x.png
INFO  Generated: fancybox/fancybox_overlay.png
INFO  Generated: fancybox/fancybox_loading@2x.gif
INFO  Generated: archives/2019/02/index.html
INFO  Generated: fancybox/fancybox_sprite.png
INFO  Generated: archives/2019/index.html
INFO  Generated: css/fonts/fontawesome-webfont.eot
INFO  Generated: js/script.js
INFO  Generated: fancybox/helpers/jquery.fancybox-buttons.js
INFO  Generated: fancybox/jquery.fancybox.pack.js
INFO  Generated: fancybox/helpers/jquery.fancybox-buttons.css
INFO  Generated: fancybox/helpers/jquery.fancybox-media.js
INFO  Generated: fancybox/helpers/jquery.fancybox-thumbs.js
INFO  Generated: css/style.css
INFO  Generated: css/fonts/fontawesome-webfont.woff
INFO  Generated: fancybox/helpers/jquery.fancybox-thumbs.css
INFO  Generated: fancybox/helpers/fancybox_buttons.png
INFO  Generated: css/fonts/FontAwesome.otf
INFO  Generated: css/images/banner.jpg
INFO  Generated: css/fonts/fontawesome-webfont.svg
INFO  Generated: css/fonts/fontawesome-webfont.ttf
INFO  Generated: 2019/02/09/hello-world/index.html
INFO  Generated: fancybox/jquery.fancybox.js
INFO  28 files generated in 834 ms
```


```
DannideMacBook-Pro:danni3 danni$ hexo server
INFO  Start processing
INFO  Hexo is running at http://localhost:4000 . Press Ctrl+C to stop.

```

```
DannideMacBook-Pro:git danni$ git clone https://github.com/ppoffice/hexo-theme-hueman
Cloning into 'hexo-theme-hueman'...
remote: Enumerating objects: 25, done.
remote: Counting objects: 100% (25/25), done.
remote: Compressing objects: 100% (22/22), done.
error: RPC failed; curl 56 SSLRead() return error -9806ytes/s   
fatal: The remote end hung up unexpectedly
fatal: early EOF
fatal: index-pack failed
```

```
DannideMacBook-Pro:danni3 danni$ hexo server 
INFO  Start processing
INFO  Hexo is running at http://localhost:4000 . Press Ctrl+C to stop.
Unhandled rejection TypeError: /Users/danni/work/svn/git/danni3/themes/hexo-theme-hueman/layout/index.ejs:1
 >> 1| <%- partial('common/archive') %>

/Users/danni/work/svn/git/danni3/themes/hexo-theme-hueman/layout/common/archive.ejs:19
    17|         <div class="article-row">
    18|     <% } %>
 >> 19|         <%- partial('summary', { post: post }) %>
    20|     <% if (postCount == page.posts.length || postCount % 2 == 0) { %>
    21|         </div>
    22|     <% } %>

/Users/danni/work/svn/git/danni3/themes/hexo-theme-hueman/layout/common/summary.ejs:3
    1| <article class="article article-summary<%= (post.direction && post.direction.toLowerCase() === 'rtl' ? ' rtl' : '') %>">
    2|     <div class="article-summary-inner">
 >> 3|         <% if (theme.customize.thumbnail === true) { %>
    4|         <%- partial('common/thumbnail', { counter: true }) %>
    5|         <% } %>
    6|         <div class="article-meta">

Cannot read property 'thumbnail' of undefined
    at eval (eval at compile (/Users/danni/work/svn/git/danni3/node_modules/ejs/lib/ejs.js:618:12), <anonymous>:13:28)
    at returnedFn (/Users/danni/work/svn/git/danni3/node_modules/ejs/lib/ejs.js:653:17)
    at Theme._View.View._compiledSync.locals [as _compiledSync] (/Users/danni/work/svn/git/danni3/node_modules/hexo/lib/theme/view.js:121:22)
    at Theme._View.View.View.renderSync (/Users/danni/work/svn/git/danni3/node_modules/hexo/lib/theme/view.js:49:23)
    at Object.partial (/Users/danni/work/svn/git/danni3/node_modules/hexo/lib/plugins/helper/partial.js:38:15)
    at eval (eval at compile (/Users/danni/work/svn/git/danni3/node_modules/ejs/lib/ejs.js:618:12), <anonymous>:47:17)
    at Model._Query.Query.Query.forEach (/Users/danni/work/svn/git/danni3/node_modules/warehouse/lib/query.js:42:5)
    at eval (eval at compile (/Users/danni/work/svn/git/danni3/node_modules/ejs/lib/ejs.js:618:12), <anonymous>:12:19)
    at returnedFn (/Users/danni/work/svn/git/danni3/node_modules/ejs/lib/ejs.js:653:17)
    at Theme._View.View._compiledSync.locals [as _compiledSync] (/Users/danni/work/svn/git/danni3/node_modules/hexo/lib/theme/view.js:121:22)
    at Theme._View.View.View.renderSync (/Users/danni/work/svn/git/danni3/node_modules/hexo/lib/theme/view.js:49:23)
    at Object.partial (/Users/danni/work/svn/git/danni3/node_modules/hexo/lib/plugins/helper/partial.js:38:15)
    at eval (eval at compile (/Users/danni/work/svn/git/danni3/node_modules/ejs/lib/ejs.js:618:12), <anonymous>:9:17)
    at returnedFn (/Users/danni/work/svn/git/danni3/node_modules/ejs/lib/ejs.js:653:17)
    at Theme._View.View._compiled.locals [as _compiled] (/Users/danni/work/svn/git/danni3/node_modules/hexo/lib/theme/view.js:125:48)
    at Theme._View.View.View.render (/Users/danni/work/svn/git/danni3/node_modules/hexo/lib/theme/view.js:30:15)
    at route.set (/Users/danni/work/svn/git/danni3/node_modules/hexo/lib/hexo/index.js:394:29)
    at tryCatcher (/Users/danni/work/svn/git/danni3/node_modules/bluebird/js/release/util.js:16:23)
    at /Users/danni/work/svn/git/danni3/node_modules/bluebird/js/release/method.js:15:34
    at RouteStream._read (/Users/danni/work/svn/git/danni3/node_modules/hexo/lib/hexo/router.js:134:3)
    at RouteStream.Readable.read (_stream_readable.js:452:10)
    at resume_ (_stream_readable.js:899:12)

```

# 绑定域名

#### 新建CNAME

- github：在sousrce下新建CNAME，填写域名：ouyangda.club，并提交到github
- gitee：登录到界面设置（要先购买pro）

#### 域名解析

- 登录阿里云或者腾讯云（或其他购买域名的地方）配置域名解析
- gitee是收费的，github是免费的
- 参考：https://blog.vinlic.com/p/50505/

#### 我的推荐

- 用github（用chrome，因为safari有些按钮点击不了）
- github成功了，但是gitee不成功，不知道为啥
- 要看github的说明，省略www、配置www、配置个性化比如blog.怎么配置都说明了
- https://help.github.com/articles/using-a-custom-domain-with-github-pages/
- github不要勾选：Enforce HTTPS，否则会提示不安全，点进去样式也乱了。点击这个马上生效的，不用保存。
- github最好不要强制https，之后再取消好像不怎么生效

# 统计

#### 页面显示uv和pv

在footer.ejs的任意地方，我选择中间，拷贝下面代码：
```
<!--这一段是不蒜子的访问量统计代码-->
<!--http://busuanzi.ibruce.info-->
<!--因七牛强制过期『dn-lbstatics.qbox.me』域名，与客服沟通无果，只能更换域名到『busuanzi.ibruce.info』-->
<!--<script async src="//dn-lbstatics.qbox.me/busuanzi/2.3/busuanzi.pure.mini.js"></script>-->
<script async src="//busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js"></script>
<span id="busuanzi_container_site_pv">本站总访问量<span id="busuanzi_value_site_pv"></span>次</span>
<span id="busuanzi_container_site_uv">访客数<span id="busuanzi_value_site_uv"></span>人次</span>
```

提交到gitee，再访问就会显示：本站总访问量1次 访客数1人次

不蒜子官网：
- http://busuanzi.ibruce.info

#### 后台统计

小蒜子只能简单的显示uv和pv，如果想复杂一点后台管理，用百度统计。

- https://tongji.baidu.com/web/welcome/login

> 注册百度统计帐号
百度统计-站长版（适合媒体站长用户）
百度统计-客户版（适合百度推广用户）
百度统计-移动统计（适合移动应用开发者）

- 选择站长版。需要重新注册。如果选择移动统计，则可以用原来百度账号，但只能是移动app使用。
- 我重新注册，用原来百度的用户名也可以的。因为是不同的东西。

- 添加你的网址
- 百度会生成一段专有的代码，粘贴

在header.ejs的`</header>`前添加：
```ejs
    <!--百度统计-->
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?d6d5fdb392be5e19c8ec8a36d7a4cb2e";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
```
检查是否安装成功

百度的检查不是很准确。用这个：
- https://tongji.baidu.com/web/help/article?id=93&type=0

chrome打开，F12，network，刷新一下，有hm.js这个文件即可。
安装成功。

> 注意：
- 每个页面都得插入，否则统计不到，最好有统一的header
- 如果浏览器点击后退，是统计不到的，因为是缓存
- 点击刷新可以统计到
- 直接点击关闭浏览器的页签，百度会认为你还在访问中的，一般会显示"未知"
